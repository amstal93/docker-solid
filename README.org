# Local IspellDict: en
#+STARTUP: showeverything

# Copyright (C) 2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC-BY-SA-4.0

* TL;DR
  1. Import [[https://gitlab.com/oer/cs/docker-solid/tree/master/certs/my_cacert.crt][CA certificate]]
     in separate browser profile.
  2. Edit your ~/etc/hosts~ to resolve host names ending in
     ~solid.localhost~ to the IP address 127.0.0.1 (or whatever IP
     address your Docker container is using).
     #+begin_src sh
     sudo -- sh -c "echo '127.0.0.1 *.localhost' >> /etc/hosts"
     sudo -- sh -c "echo '127.0.0.1 *.solid.localhost' >> /etc/hosts"
     #+end_src
     Try ~ping solid.localhost~ with the above settings.
     If that works, everything is fine.  If not, either
     add entries without wildcard (i.e., without ~*~, such as
     ~solid.localhost~, ~alice.solid.localhost~,
     ~bob.solid.localhost~, …) or install ~dnsmasq~.
     Try ~ping~ again.
  3. Start server with Docker (maybe replace ~$PWD~ with a directory of
     your choice, where you want to collect Solid’s data):
     #+begin_src sh
     docker run -it --cap-add=NET_ADMIN --dns=127.0.0.1 -p 8443:8443 --name my-solid -v $PWD:/opt/solid registry.gitlab.com/oer/cs/docker-solid/docker-solid
     #+end_src
  4. Register new Solid user(s) on your own POD:
     [[https://solid.localhost:8443]]
  5. Explore your POD.
     [[http://dontai.com/wp/2018/10/01/playing-with-solids-pod-documentation/][Some pointers]].

* Background
[[https://en.wikipedia.org/wiki/Solid_(web_decentralization_project)][Solid]]
(SOcial LInked Data) is a project for more decentralization on the
[[https://oer.gitlab.io/oer-courses/vm-neuland/02-Web.html][Web]].
[[https://github.com/solid/node-solid-server][Solid servers]]
(so-called PODS, personal online data stores) are special Web servers
to share, interact with, and collaborate on
[[https://programminghistorian.org/en/lessons/intro-to-linked-data][linked data]].
Users can register identities (so-called WebIDs) on Solid servers of
their choice, where users define how to share their data and with whom.

For experiments with Solid, one can register a WebID at a
[[https://github.com/solid/solid#get-a-webid][public server]]
or [[https://github.com/solid/node-solid-server#install][install a local server]].
The latter choice has several advantages when /learning/ with and about Solid:
1. No need to share data with unknown parties
2. Ability to inspect the local filesystem, to see how resources and
   interactions are managed
3. Learn about server administration including
   - Name resolution
   - TLS certificates

To simplify the installation of a local server, this repository
provides a Docker image (see
[[https://oer.gitlab.io/oer-courses/vm-neuland/04-Docker.html][here for an introduction to Docker]])
with a Solid server that is accessible on [[https://solid.localhost:8443]].
The wildcard TLS certificate for ~*.solid.localhost~ installed on that
server was created by a test certificate authority (CA), whose key
material is also included in this repository.
The process to generate the CA and associated certificates is
documented
[[https://oer.gitlab.io/oer-courses/vm-neuland/texts/05-create-root-CA.html][elsewhere]].

* Create and use Docker image
The Docker image in this repository is defined by two files,
[[https://gitlab.com/oer/cs/docker-solid/blob/master/Dockerfile][Dockerfile]]
and [[https://gitlab.com/oer/cs/docker-solid/blob/master/entrypoint.sh][entrypoints.sh]],
which were inspired by (but do no longer share much resemblance with) files coming with
[[https://github.com/angelo-v/docker-solid-server][this Docker image]].

You can either build the image locally or download it from a registry.

** Local image
The Docker image can be built as usual (~docker build -t docker-solid -f Dockerfile .~).
Start Solid as follows:
#+begin_src sh
docker run -it --cap-add=NET_ADMIN --dns=127.0.0.1 -p 8443:8443 --name my-solid -v $PWD:/opt/solid docker-solid
#+end_src
The options ~--cap-add=NET_ADMIN --dns=127.0.0.1~ are necessary to
provide network access to ~dnsmasq~ and DNS resolution on localhost (a
warning about DNS on localhost is shown, but this works for me).

** Image from remote registry
Use the Docker image from GitLab’s registry:
#+begin_src sh
docker run -it --cap-add=NET_ADMIN --dns=127.0.0.1 -p 8443:8443 --name my-solid -v $PWD:/opt/solid registry.gitlab.com/oer/cs/docker-solid/docker-solid
#+end_src

* Use your POD
** Set up name resolution
Set up your own operating system to resolve names ending in
~solid.localhost~ to the IP address of the docker container (typically
127.0.0.1; maybe 192.168.99.100).  The following might be enough on GNU/Linux:
#+begin_src sh
sudo -- sh -c "echo '127.0.0.1 *.localhost' >> /etc/hosts"
sudo -- sh -c "echo '127.0.0.1 *.solid.localhost' >> /etc/hosts"
#+end_src
If that does not work, either add full names (without wildcard) or use ~dnsmasq~; see
[[https://gitlab.com/oer/cs/docker-solid/blob/master/Dockerfile][Dockerfile]]
and [[https://gitlab.com/oer/cs/docker-solid/blob/master/entrypoint.sh][entrypoints.sh]]
for what happens inside the Docker image.  On other systems, startpage
for instructions, e.g.,
[[https://www.startpage.com/do/search?q=edit+hosts+file+windows][for Windows]].

** Import CA certificate
To avoid browser warnings, import the
[[https://gitlab.com/oer/cs/docker-solid/tree/master/certs/my_cacert.crt][CA certificate]]
used inside the container.
(E.g., download that file.  Then, with Firefox: Preferences → Privacy
& Security → Certificates → View Certificates → Authorities → Import)

As the private key for that CA is published in this repository,
/anyone/ can impersonate the CA and issue certificates for /any/
domain, which is why the certificate should be installed in a
separate browser profile for testing purposes.

** Use POD
Register new Solid user(s) on your own POD:
[[https://solid.localhost:8443]]

Some pointers for Solid’s functionality are
[[http://dontai.com/wp/2018/10/01/playing-with-solids-pod-documentation/][provided in this blog post]].

* Clean up
If you imported the above CA certificate into your browser, make sure
to delete it at the end of your experiments.  View certificates (as
for import above), scroll down to “University of Muenster”, delete
“Solid operator”.
