#!/bin/sh
# Copyright (C) 2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

set -e

# # Add entries to redirect hostnames ending in "localhost" to 127.0.0.1:
# echo "127.0.0.1 *.localhost" >> /etc/hosts
# echo "127.0.0.1 *.solid.localhost" >> /etc/hosts

# # Make sure that /etc/hosts is used:
# echo "hosts: files dns" >> /etc/nsswitch.conf

# The previous configuration in comments does not work with wildcards in the
# alpine based container.  No idea, why.  Use dnsmasq instead:
echo "address=/solid.localhost/127.0.0.1" >> /etc/dnsmasq.conf
dnsmasq

# Add own CA with node:
# https://nodejs.org/api/cli.html#cli_node_extra_ca_certs_file
export NODE_EXTRA_CA_CERTS=/tmp/certs/my_cacert.crt

solid "$@"
